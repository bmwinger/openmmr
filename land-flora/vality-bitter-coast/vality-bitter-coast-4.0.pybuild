# Copyright 2022 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from pybuild.info import P

from common.mw import MW, File, InstallDir
from common.util import CleanPlugin


class Package(CleanPlugin, MW):
    NAME = "Vality's Bitter Coast"
    DESC = "Replaces all the 'leafy' BC trees and adds many more trees to the BC to give the region a more dense atmosphere."
    HOMEPAGE = "https://web.archive.org/web/20161103115849/http://mw.modhistory.com/download-56-11539"
    KEYWORDS = "openmw"
    IUSE = "graht-swamp-trees"
    LICENSE = "all-rights-reserved"
    RDEPEND = """
        base/morrowind
        graht-swamp-trees? ( land-flora/graht-morrowind-swamp-trees )
    """
    SRC_URI = f"""
        https://web.archive.org/web/20161103115849/http://mw.modhistory.com/file.php?id=11539 -> {P}.7z
        graht-swamp-trees? ( https://mw.moddinghall.com/file/69-graht-morrowind-swamp-trees/?do=download -> Graht_Morrowind_Swamp_Trees-49771-2-4-1649099371.7z )
    """
    INSTALL_DIRS = [
        InstallDir(
            ".",
            BLACKLIST=["Vality's Bitter Coast Trees - Readme.txt"],
            PLUGINS=[File("Vality's Bitter Coast Addon.esp", CLEAN=True)],
            REQUIRED_USE="!graht-swamp-trees",
            S=P,
        ),
        InstallDir(
            ".",
            BLACKLIST=[
                "Vality's Bitter Coast Addon.esp",
                "Vality's Bitter Coast Trees - Readme.txt",
            ],
            REQUIRED_USE="graht-swamp-trees",
            S=P,
        ),
        InstallDir(
            "02 Vality BC Patch",
            PLUGINS=[File("GMST Vality's Bitter Coast Addon.esp")],
            REQUIRED_USE="graht-swamp-trees",
            S="Graht_Morrowind_Swamp_Trees-49771-2-4-1649099371",
        ),
    ]
