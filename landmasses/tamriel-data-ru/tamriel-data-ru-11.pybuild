# Copyright 2019-2023 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Tamriel Data Ru"
    DESC = " This mod provides russian localization for Tamriel Data "
    HOMEPAGE = """
        https://github.com/TD-Addon/TD_Addon
        https://www.nexusmods.com/morrowind/mods/55464
        https://www.nexusmods.com/morrowind/mods/55805
    """
    LICENSE = "tamriel-data-10.0"
    # weapon-sheathing-tr is included
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        !!gameplay-weapons/weapon-sheathing-tr
    """
    MORROWIND_COMPAT = "ru"
    IUSE = "tr pc"
    KEYWORDS = "openmw"
    # there are multiple separate efforts to translate mods which use tamriel data
    # until there is some consensus on which translation is the best one, lets have them both
    NEXUS_SRC_URI = """
        tr? ( https://www.nexusmods.com/morrowind/mods/55464?tab=files&file_id=1000050409 -> trrus24-55464-2025-02-15-1739615975.7z )
        pc? ( https://www.nexusmods.com/morrowind/mods/55805?tab=files&file_id=1000049602 -> Cyr_rus-24-12a-2025-01-23.7z-55805-24-12b-3-1737583124.7z )
    """
    REQUIRED_USE = "^^ ( tr pc )"
    TIER = 2

    INSTALL_DIRS = [
        InstallDir(
            "trrus24_2025-02-15/data files",
            S="trrus24-55464-2025-02-15-1739615975",
            WHITELIST="Tamriel_*",
            PLUGINS=[
                File("Tamriel_Data.esm"),
            ],
            REQUIRED_USE="tr",
        ),
        InstallDir(
            "00 Core",
            S="Cyr_rus-24-12a-2025-01-23.7z-55805-24-12b-3-1737583124",
            WHITELIST="Tamriel_*",
            PLUGINS=[
                File("Tamriel_Data.esm"),
            ],
            REQUIRED_USE="pc",
        ),
    ]
