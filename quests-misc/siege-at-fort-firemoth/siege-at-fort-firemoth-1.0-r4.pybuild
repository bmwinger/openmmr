# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os

from common.mw import MW, File, InstallDir


class Package(MW):
    NAME = "Siege at Firemoth"
    DESC = "Adds a small island chain and a quest involving a large army of skeletons"
    HOMEPAGE = """
        https://elderscrolls.bethesda.net/en/morrowind
        https://gitlab.com/bmwinger/umopp
        darker? ( https://www.nexusmods.com/morrowind/mods/47127 )
    """
    # Original is all-rights-reserved
    # UMOPP is attribution
    LICENSE = "all-rights-reserved attribution"
    RESTRICT = "mirror"
    # Not necessarily a hard blocker for morrowind-rebirth, but the minimal version
    # increases compatability and should be used with it
    RDEPEND = """
        base/morrowind
        !minimal? ( !!base/morrowind-rebirth[-firemoth] )
    """
    DEPEND = ">=bin/delta-plugin-0.15 <bin/delta-plugin-0.19"
    KEYWORDS = "openmw"
    IUSE = "minimal darker"
    SRC_URI = """
        https://cdn.bethsoft.com/elderscrolls/morrowind/other/firemoth1.1.zip
        https://gitlab.com/bmwinger/umopp/uploads/2877e64ccfc6b6b178bddca111f9e8d4/firemoth-umopp-3.2.0.tar.xz
        darker? ( https://gitlab.com/portmod-mirrors/openmw/-/raw/master/Darker_Morrowind_Mods-47127-1-0-2-1584666728.7z )
    """
    INSTALL_DIRS = [
        InstallDir(
            "Data Files", PLUGINS=[File("Siege at Firemoth.esp")], S="firemoth1.1"
        ),
        InstallDir(
            ".",
            S="Darker_Morrowind_Mods-47127-1-0-2-1584666728",
            PLUGINS=[File("Darker Firemoth.esp")],
            REQUIRED_USE="darker",
            WHITELIST=["Darker Firemoth.esp"],
        ),
    ]

    def src_prepare(self):
        # From instructions in README.md
        os.chdir("Data Files")
        path = os.path.join(self.WORKDIR, "firemoth-umopp-3.2.0")
        if "minimal" in self.USE:
            self.execute(
                "delta_plugin -v apply "
                + os.path.join(path, "Siege_at_Firemoth-compat.patch")
            )
        else:
            self.execute(
                "delta_plugin -v apply " + os.path.join(path, "Siege_at_Firemoth.patch")
            )
