# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Better Sounds"
    DESC = "Changes default sound effects"
    HOMEPAGE = """
        https://www.nexusmods.com/morrowind/mods/9967
        https://modding-openmw.com/mods/better-sounds-11-patch2/
        https://www.nexusmods.com/morrowind/mods/48955
    """
    KEYWORDS = "openmw"
    LICENSE = "all-rights-reserved"
    # Note: conflict with ncgd and mbsp in that destruction skill will not level up
    RDEPEND = """
        base/morrowind[tribunal,bloodmoon]
        !!gameplay-advancement/mbsp-2.1
    """
    NEXUS_SRC_URI = """
       https://www.nexusmods.com/morrowind/mods/9967?tab=files&file_id=8352
       -> BetterSounds-9967.zip
       https://www.nexusmods.com/morrowind/mods/9967?tab=files&file_id=19883
       -> BetterSounds_Patch_v1_1-9967.rar
    """
    SRC_URI = """
        https://modding-openmw.com/files/Better_Sounds_1.1_Patch_2.7z
        https://gitlab.com/portmod-mirrors/openmw/-/raw/master/Better_Sounds_-_Yet_Another_Patch_-_Expanded-48955-1-1-1-1640535935.7z
    """
    DATA_OVERRIDES = "media-audio/bitter-coast-sounds"
    SETTINGS = {"Sound": {"buffer cache max": "64"}}
    IUSE = "taverns-and-towns"

    INSTALL_DIRS = [
        InstallDir(
            "Data Files",
            S="BetterSounds-9967",
            BLACKLIST=["Better_Sounds.esp"],
        ),
        InstallDir(
            "Data Files",
            S="BetterSounds_Patch_v1_1-9967",
            PLUGINS=[File("BS_BM_WeatherChange.esp")],
            BLACKLIST=["Better_Sounds.esp"],
        ),
        InstallDir(
            ".",
            S="Better_Sounds_1.1_Patch_2",
            PLUGINS=[
                File("Better_Sounds_1.1_Patch_2.omwaddon", OVERRIDES="bcsounds.esp")
            ],
        ),
        InstallDir(
            "01 Patch",
            S="Better_Sounds_-_Yet_Another_Patch_-_Expanded-48955-1-1-1-1640535935",
            PLUGINS=[File("Yet Another Better Sounds Patch.esp")],
            REQUIRED_USE="!taverns-and-towns",
        ),
        InstallDir(
            "02 Tavern and Town Sounds Expansion",
            S="Better_Sounds_-_Yet_Another_Patch_-_Expanded-48955-1-1-1-1640535935",
            PLUGINS=[
                File(
                    "Yet Another Better Sounds Patch - Tavern and Town Sounds Expansion.esp"
                )
            ],
            REQUIRED_USE="taverns-and-towns",
        ),
    ]
